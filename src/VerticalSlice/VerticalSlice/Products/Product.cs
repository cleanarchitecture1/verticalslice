﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerticalSlice.Categories;
using VerticalSlice.Data;

namespace VerticalSlice.Products
{
    public class Product : Model
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Guid CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
