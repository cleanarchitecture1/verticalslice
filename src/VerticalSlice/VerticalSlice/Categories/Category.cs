﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VerticalSlice.Data;
using VerticalSlice.Products;

namespace VerticalSlice.Categories
{
    public class Category : Model
    {
        public string Name { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}
