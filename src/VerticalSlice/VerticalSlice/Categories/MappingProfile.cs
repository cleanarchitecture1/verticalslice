﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using VerticalSlice.Data;

namespace VerticalSlice.Categories
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Guid, Category>()
                .ConvertUsing<GuidToCategoryConverter>();
        }
    }

    public class GuidToCategoryConverter : ITypeConverter<Guid, Category>
    {
        private readonly DataContext _db;

        public GuidToCategoryConverter(DataContext db)
        {
            _db = db;
        }

        public Category Convert(Guid source, Category destination, ResolutionContext context)
        {
            return _db.Categories.FirstOrDefault(c => c.Id == source);
        }
    }
}
