﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentValidation.Results;
using VerticalSlice;

namespace FluentValidation
{
    public static class ValidationResultExtensions
    {
        public static Result<T> ToResult<T>(this ValidationResult validationResult)
        {
            var result = new Result<T>
            {
                Code = ResultCode.BadRequest,
                Errors = validationResult.Errors.Select(v => new ErrorMessage()
                {
                    PropertyName = v.PropertyName,
                    Message = v.ErrorMessage
                })
            };

            return result;
        }
    }
}
